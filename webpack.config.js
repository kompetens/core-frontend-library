let path              = require('path');
let webpack           = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

/**
 * Export the webpack configuration
 */
module.exports = {
    entry:   {
        app: [
            './demo/src/app.js',
            './demo/src/app.scss'
        ],
    },
    output:  {
        path:     path.resolve(__dirname, './demo/build'),
        filename: '[name].js',
    },
    module:  {
        rules: [
            {
                test: /\.css$/,
                use:  ['style-loader', 'css-loader']
            },
            {
                test: /\.s[ac]ss$/,
                use:  ExtractTextPlugin.extract(
                    {
                        fallback: 'style-loader',
                        use:      ['css-loader', 'sass-loader']
                    }
                )
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({filename: '[name].css'}),
        new webpack.ProvidePlugin({
            $:      'jquery',
            jQuery: 'jquery',
            Popper: "popper.js"
        })
    ],
    devtool: '#eval-source-map'
};
