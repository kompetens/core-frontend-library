/**
 * Store constants here in order to have one single place to defined them.
 * This makes it hard to use the wrong one if we need to know a certain value
 * or CSS class anywhere in the application. Also makes renaming super
 * easy virtually removing any unwanted side effects.
 */

/**
 * Key used for localStorage when storing and retrieving session data.
 */
export const SESSION_STORAGE_KEY = 'session_data';
export const SETTINGS_STORAGE_KEY = 'settings_data';
export const ALERT_STORAGE_KEY   = 'alerts';

/**
 * Search constants
 */
export const SEARCH_CLASS_OPEN      = 'search--open';
export const SEARCH_CLASS_ERROR     = 'search--error';
export const SEARCH_CLASS_SEARCHING = 'search--searching';

/**
 * Navigation constants
 */
export const SIDEBAR_TOGGLE_CLASS = 'top-bar__toggle';
export const SIDEBAR_CLASS        = 'sidebar';
export const SIDEBAR_OPEN_CLASS   = 'open-sidebar';

/**
 * Event names
 */
export const SETTINGS_LOADED = 'settings-loaded';
export const USER_LOADED     = 'user-loaded';
export const CORE_LOADED     = 'core-loaded';
