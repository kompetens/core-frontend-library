import get from "lodash/get";
import { SETTINGS_LOADED, SETTINGS_STORAGE_KEY } from "./constants";

/**
 * Load the settings by first getting any cached object and then load the most
 * recent settings from the server.
 */
export default function settings() {

    /**
     * Set an empty settings object stored in the window until we have loaded
     * the actual object.
     */
    window.Settings = {
        loaded:                    false,
        display_injected_managers: true,
        currency:                  'sek',
        localization:              defaultLocalization(),
        tags:                      [],
        segments:                  [],
        team_roles:                [],
        package_types:             [],
        package_categories:        []
    };

    // Time we will deem acceptable to cache the settings without issuing a re-fetch
    let cacheDuration = 5 * 60 * 1000; // 5 minutes
    let needsReFetch  = true;

    /**
     * Attempt to read cached settings from localStorage.
     * Also check if the cached settings were stored within the cache duration.
     */
    let storedSettings = localStorage.getItem(SETTINGS_STORAGE_KEY);
    if (storedSettings) {
        try {
            let parsed   = JSON.parse(storedSettings);
            needsReFetch = parsed.ts < (Date.now() - cacheDuration);
            fillSettings(parsed.data);
        } catch (e) {
            //
        }
    }

    /**
     * Send the request to fetch the settings.
     */
    if (needsReFetch) {
        window.Ajax.get('settings')
              .then(response => setAndCacheSettings(response.data))
              .catch((response, status) => {
                  if (status !== 0 && status !== 401) {
                      console.error(response);
                      throw `[${status}] Error loading settings: ${response.message}`;
                  }
              });
    }
}

/**
 * Replace the cached settings object.
 *
 * @param {Object} settings
 */
export function setAndCacheSettings(settings) {
    fillSettings(settings);
    localStorage.setItem(
        SETTINGS_STORAGE_KEY,
        JSON.stringify({
            ts:   Date.now(),
            data: settings
        })
    );
}

/**
 *
 * @returns {Object}
 */
function defaultLocalization() {
    return {
        separator:       ' ',
        decimal_point:   ',',
        currency_symbol: 'kr',
        suffix_currency: true,
    };
}

/**
 *
 * @param {Object} data
 */
function fillSettings(data) {
    window.Settings.display_injected_managers = !!get(data, 'display_injected_managers', true);
    window.Settings.currency                  = get(data, 'currency', 'sek');
    window.Settings.localization              = get(data, 'localization', defaultLocalization());
    window.Settings.tags                      = get(data, 'tags', []);
    window.Settings.segments                  = get(data, 'segments', []);
    window.Settings.team_roles                = get(data, 'team_roles', []);
    window.Settings.package_types             = get(data, 'package_types', []);
    window.Settings.package_categories        = get(data, 'package_categories', []);

    // Set the settings flag to be loaded and emit the loaded event.
    window.Settings.loaded = true;
    $(document).trigger(SETTINGS_LOADED);
}
