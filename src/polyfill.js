import promisePolyfill from "es6-promise";

/**
 * Polyfills to improve compatibility with older browsers.
 * Only the newer browsers natively support promises but
 * the rest of the polyfills are there for IE9 and 10.
 */
export default function polyfill() {

    // Promise polyfill
    promisePolyfill.polyfill();

    /**
     * Returns only the unique elements in an array.
     *
     * @returns {Array}
     */
    Array.prototype.unique = function () {
        let unique = {};
        let result = [];

        for (let i = 0, l = this.length; i < l; ++i) {
            if (unique.hasOwnProperty(this[i])) {
                continue;
            }
            result.push(this[i]);
            unique[this[i]] = 1;
        }

        return result;
    };

    /**
     * Polyfill for Object.assign()
     */
    if (typeof Object.assign !== 'function') {
        (function () {
            Object.assign = function (target) {
                'use strict';
                // We must check against these specific cases.
                if (target === undefined || target === null) {
                    throw new TypeError('Cannot convert undefined or null to object');
                }

                let output = Object(target);
                for (let index = 1; index < arguments.length; index++) {
                    let source = arguments[index];
                    if (source !== undefined && source !== null) {
                        for (let nextKey in source) {
                            if (source.hasOwnProperty(nextKey)) {
                                output[nextKey] = source[nextKey];
                            }
                        }
                    }
                }
                return output;
            };
        })();
    }

    /**
     * Polyfill for Array.find()
     */
    if (!Array.prototype.find) {
        Object.defineProperty(Array.prototype, "find", {
            value: function (predicate) {
                'use strict';
                if (this === null) {
                    throw new TypeError('Array.prototype.find called on null or undefined');
                }
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }
                let list    = Object(this);
                let length  = list.length >>> 0;
                let thisArg = arguments[1];
                let value;

                for (let i = 0; i < length; i++) {
                    value = list[i];
                    if (predicate.call(thisArg, value, i, list)) {
                        return value;
                    }
                }
                return undefined;
            }
        });
    }

}
