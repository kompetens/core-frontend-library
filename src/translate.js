import get from "lodash/get";

export default function translator() {
    window.trans = function (key, attributes = {}) {
        let parts       = key.split('.');
        let translation = null;
        let step        = window.Translations;

        /**
         * Go through each part of the translation name and
         */
        for (let i = 0; i < parts.length; i++) {
            let part = parts[i];
            if (part in step && typeof step[part] === 'string' && (i + 1) === parts.length) {
                translation = step[part];
            } else if (part in step && typeof step[part] === 'object') {
                step = step[part];
            }
        }

        /**
         * If no translation was found we return the key instead.
         * This way we don't render empty strings, and
         * provide a way to find missing translations.
         */
        if (!translation) {

            let obj = get(window.Translations, key, null);
            if (obj !== null) {
                return obj;
            }

            return key;
        }

        /**
         * Replace dynamic variables.
         */
        if (translation && translation.search(':') !== -1) {
            let regex   = /(?:^|\W):(\w+)(?!\w)/g;
            let match;
            let matches = [];
            while (match = regex.exec(translation)) {
                matches.push(match[1]);
                if (match[1] in attributes) {
                    translation = translation.replace(':' + match[1], attributes[match[1]]);
                }
            }
        }

        return translation;
    };
}
