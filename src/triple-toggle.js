(function () {
    $(document).ready(_ => $('.triple-input').each((i, el) => new TripleToggle($(el))));
})();

/**
 *
 */
export class TripleToggle {

    /**
     * Pass in the jquery element of the toggle "container"
     *
     * @param {*} el
     */
    constructor(el) {
        this.el    = el;
        this.value = null;

        // Listen for changes of the value.
        el.find('.triple-toggle__option').on('click', e => {
            this.setValue($(e.target).attr('data-value'));
        });

        // TODO: handle selector drag?
    }

    /**
     * Set a new value for the toggle. Automatically moves the selector to
     * update the visual of what is selected.
     *
     * @param   {string|boolean} val
     * @returns {boolean|null}
     */
    setValue(val) {
        // Parse the value
        let {value, strValue} = TripleToggle.parsedValue(val);
        this.value            = value;

        // Reset css class value and add the new state.
        this.el.attr('class', 'triple-toggle');
        this.el.addClass('triple-toggle--' + strValue);

        // Return the value should we want to use the value immediately.
        return this.value;
    }

    /**
     *
     * @param   {string|boolean} input
     * @returns {{value: *, strValue: string}}
     */
    static parsedValue(input) {
        let value    = null;
        let strValue = 'null';

        if (input === true || input === 'true') {
            value    = true;
            strValue = 'true';
        } else if (input === false || input === 'false') {
            value    = false;
            strValue = 'false';
        }

        return {value, strValue};
    }
}
