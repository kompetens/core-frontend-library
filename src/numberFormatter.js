export default function numberFormatter() {
    /**
     * Format a number localized.
     *
     * @param  {Number} value
     * @param  {Number} decimals
     * @return {String}
     */
    window.formatNumber = function (value, decimals = 0) {
        // TODO: Can we set the decimal point here?
        // We'd like to use window.Config.settings.localization.decimal_point
        return value.toFixed(decimals).replace(/\B(?=(\d{3})+(?!\d))/g, window.Settings.localization.separator);
    };

    /**
     * Format a currency localized.
     *
     * @param  {Number} value
     * @param  {Number} decimals
     * @param  {String} denominator
     * @return {String}
     */
    window.formatCurrency = function (value, decimals = 0, denominator = null) {
        if (!denominator) denominator = window.Settings.localization.currency_symbol;
        let formatted = window.formatNumber(value, decimals);

        return window.Settings.localization.suffix_currency
            ? formatted + ' ' + denominator
            : denominator + ' ' + formatted;
    };
}
