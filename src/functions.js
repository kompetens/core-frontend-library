import get from "lodash/get";

/**
 * Check if an element is a child of any element with the given class.
 *
 * @param  {HTMLElement} element
 * @param  {string}      className
 * @return {boolean}
 */
export function partOf(element, className) {
    let isPart = false;
    let check  = element;

    while (check.parentNode !== null) {
        if (check.classList.contains(className)) {
            isPart = true;
            break;
        }

        check = check.parentNode;
    }

    return isPart;
}

/**
 * Get a cookie or an object containing all cookies.
 *
 * @param  {string|null} key
 * @return {string|Object}
 */
export function cookies(key = null) {
    let rawCookies = document.cookie ? document.cookie.split('; ') : [];
    let cookies    = {};

    for (let i = 0; i < rawCookies.length; i++) {
        let parts  = rawCookies[i].split('=');
        let cookie = parts.slice(1).join('=');

        if (cookie.charAt(0) === '"') {
            cookie = cookie.slice(1, -1);
        }

        let name = parts[0];

        cookies[name] = cookie;
    }

    if (key === null) {
        return cookies;
    }

    return cookies[key];
}

/**
 * Safely build an URL to use for the AJAX request. If the endpoint starts with
 * http we simply return it as is. Else we prepend the API url as the base URL,
 * safely setting the forward slashes between the parts.
 *
 * @param  {string} endpoint
 * @param  {?string} targetApi
 * @param  {?Object} params
 * @return {string}
 */
export function urlToEndpoint(endpoint, targetApi = null, params = null) {
    if (endpoint.indexOf('http') === 0) {
        return endpoint;
    }

    let baseUrl = window.Config.apps.api.url;
    if (targetApi !== null && targetApi in window.Config.apps) {
        baseUrl = window.Config.apps[targetApi].url;
    }

    if (baseUrl.indexOf('/') !== (baseUrl.length - 1)) {
        baseUrl += '/';
    }

    if (endpoint.indexOf('/') === 0) {
        endpoint = endpoint.slice(1);
    }

    /**
     * Attach any extra query params to the URL
     */
    if (params !== null) {
        let query = $.param(params);
        endpoint += (endpoint.indexOf('?') !== -1 ? '&' : '?') + query;
    }

    return baseUrl + endpoint;
}

/**
 * Get the contents of the file in a file input as base64.
 * The input needs to be a jQuery object containing the input element.
 * This only fetches the first file and does not work on multiple file inputs.
 *
 * @param {Object}   inputElement
 * @param {function} callback
 */
export function contentsFromFileInput(inputElement, callback) {
    let files = inputElement.prop('files');

    if (files.length) {
        let reader    = new FileReader();
        reader.onload = e => callback(e.target.result);
        reader.readAsDataURL(files[0]);

    } else {
        callback(null);
    }
}

/**
 *
 * @param {HTMLElement} input
 * @param {function} callback
 */
export function getFileAndSource(input, callback) {
    if (!input.files.length) return;
    let file   = input.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = e => {
        let src = e.target.result;
        callback({src, file});
    };
}

/**
 *
 * @param   {*} input
 * @returns {*}
 */
export function convertBoolAndNull(input) {
    if (input === true) {
        return 1;
    } else if (input === false) {
        return 0;
    }
    return '';
}

/**
 * Take a value and make sure to return a number, using a fallback value
 * if the provided value cannot be returned as a number.
 *
 * @param  {*}      value
 * @param  {number} fallback
 * @return {number}
 */
export function getAsNumber(value, fallback = 0) {
    if (typeof value === 'number') {
        return value;
    }

    let parsed = parseFloat(value);
    return isNaN(parsed) ? getAsNumber(fallback) : parsed;
}


/**
 * Take a value and make sure to return as an array
 *
 * @param  {*}     value
 * @param  {Array} fallback
 * @return {Array}
 */
export function getAsArray(value, fallback = []) {
    return value instanceof Array ? value : fallback;
}

/**
 * Convert new lines to HTML breaks
 *
 * @param  {string} str
 * @return {string}
 */
export function nl2br(str) {
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
}

/**
 * Helper to check if an application is enabled.
 *
 * @param   {string} name
 * @returns {boolean}
 */
export function appIsEnabled(name) {
    return get(window, `Config.apps.${name}.enabled`) === true;
}
