import { ALERT_STORAGE_KEY } from "./constants";

export default function alerts(container) {

    /**
     * Add an object to the window that contains some helpers to add the alert messages.
     *
     * @type {Object}
     */
    window.Alert = {

        // Add a "success" alert
        success(message, title = null, timeout = 5000) {
            buildAlert(message, 'success', title, timeout);
        },

        // Add a "danger" alert
        danger(message, title = null, timeout = 10000) {
            buildAlert(message, 'danger', title, timeout);
        },

        // Add a "info" alert
        info(message, title = null, timeout = 5000) {
            buildAlert(message, 'info', title, timeout);
        },

        // Add a "warning" alert
        warning(message, title = null, timeout = 5000) {
            buildAlert(message, 'warning', title, timeout);
        },

        // Emit a "generic" alert. Used when sending alerts from storage.
        emit(type, message, title = null, timeout = 5000) {
            buildAlert(message, type, title, timeout);
        },

        // Queue up an alert to be sent on the next page load.
        queue(type, message, title = null, timeout = 5000) {
            let alert = {type, message, title, timeout};

            // Get any existing alerts should there be any
            let storedData = sessionStorage.getItem(ALERT_STORAGE_KEY);
            let list       = [];
            if (storedData) {
                list = JSON.parse(storedData);
            }

            // Add the alert to the list and store in session storage.
            list.push(alert);
            sessionStorage.setItem(ALERT_STORAGE_KEY, JSON.stringify(list));
        }
    };

    /**
     * Show any alerts put in session storage This way we can set an alert before
     * redirecting to another page, for example when redirecting from the create
     * form to the edit page. We use sessionStorage instead of localStorage to
     * prevent alerts "leaking" between tabs.
     */
    let alerts = sessionStorage.getItem(ALERT_STORAGE_KEY);
    sessionStorage.removeItem(ALERT_STORAGE_KEY);
    if (alerts) {
        alerts = JSON.parse(alerts);
        alerts.forEach(alert => window.Alert.emit(
            alert.type,
            alert.message,
            alert.title,
            alert.timeout
        ));
    }

    /**
     * Build an alert element and add it to the alert container.
     *
     * @param  {string} message
     * @param  {string} type
     * @param  {string} title
     * @param  {Number} timeout
     */
    function buildAlert(message, type, title, timeout) {

        /**
         * Build the main alert element.
         */
        let alert = $(`<div class="alert alert-${type}"></div>`);

        /**
         * Store a timeout id to allow clearing the timeout for the alert if removed
         * by the user before the automatic timeout has passed and attempts to remove
         * the alert. Otherwise it would try to remove an element that does not exist.
         */
        let alertTimeout = null;

        /**
         * Create a function that removes the alert from the DOM. It is useful to
         * save this function as it is used both when clicking the button
         * to close the alert as well as when the timeout has passed.
         */
        const removeAlert = function () {
            alert.remove();
            clearTimeout(alertTimeout);
        };

        /**
         * If there is a timeout set (in milliseconds) we remove the alert after the timeout.
         */
        if (parseInt(timeout) > 0) {
            alertTimeout = setTimeout(removeAlert, timeout);
        }

        /**
         * Build the button used to close the alert and attach the event listener that closes the alert.
         */
        let closeButton = $(`<button class="close" type="button"><span aria-hidden="true">&times;</span></button>`);
        closeButton.on('click', removeAlert);
        alert.append(closeButton);

        /**
         * Create the title element and add it, but only if a title was supplied.
         */
        if (title !== null) {
            alert.append(`<p><strong>${title}</strong></p>`);
        }

        /**
         * Create the paragraph tag containing the actual message and add it to the alert.
         */
        alert.append(`<p>${message}</p>`);

        /**
         * Append the alert to the container.
         */
        container.append(alert);
    }
}
