import get from "lodash/get";
import { urlToEndpoint } from "./functions";

/**
 * Export the setup function for the Ajax wrapper
 * Accepts an options object for some basic customization.
 *
 * @param   {Object} options
 * @returns {*}
 */
export default function ajax(options = {}) {

    // Get the data from the options object
    const tokenResolver = get(options, 'tokenResolver');
    const authCallback  = get(options, 'authenticationCallback');

    // Make the ajax wrapper available.
    window.Ajax = {

        /**
         * Wrap a jQuery ajax request for the API.
         *
         * @param  {string}   method
         * @param  {string}   endpoint
         * @param  {Object}   data
         * @return {Promise}
         */
        request(method, endpoint, data = null) {

            let options = {
                method:      method,
                contentType: 'application/json',
                headers:     {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Core-Token':       tokenResolver()
                }
            };

            if (data !== null) {
                options.data = data;

                if (method.toLowerCase() !== 'get') {
                    options.data     = JSON.stringify(data);
                    options.dataType = 'json';
                }
            }

            // Return a promise to complete the request.
            return new Promise(function (resolve, reject) {

                /**
                 * Only responses with the status message of "success" are deemed
                 * a successful response. All others will be "rejected"
                 *
                 * @param {*}      data
                 * @param {string} message
                 * @param {Object} jqXHR
                 */
                options.success = function (data, message, jqXHR) {
                    if (data.status === 'success') {
                        resolve(data, jqXHR.status, message);
                    } else {
                        reject(data, jqXHR.status, message);
                    }
                };

                /**
                 * Handle errors. Use the 401 callback if necessary and Since jQuery
                 * does not parse the response on "failed" requests we must do so manually.
                 *
                 * @param {Object} jqXHR
                 * @param {string} message
                 */
                options.error = function (jqXHR, message) {

                    // Build a fallback response object
                    let response = {
                        status:  'error',
                        message: 'Network error',
                        data:    {}
                    };

                    // Attempt to parse the server response.
                    try {
                        response = JSON.parse(jqXHR.responseText);
                    } catch (e) {
                        response.message = "Unable to parse response data";
                    }

                    // If we have a callback for authentication errors we only execute that.
                    if (jqXHR.status === 401 && typeof authCallback === 'function') {
                        authCallback(response);
                    } else {
                        reject(response, jqXHR.status, message);
                    }
                };

                // Send the request
                $.ajax(urlToEndpoint(endpoint), options);
            });
        },

        /**
         * Send a GET request
         *
         * @param  {string}   endpoint
         * @param  {Object}   params
         * @return {Promise}
         */
        get(endpoint, params = null) {
            return window.Ajax.request('GET', endpoint, params);
        },

        /**
         * Send a POST request
         *
         * @param  {string}   endpoint
         * @param  {Object}   data
         * @return {Promise}
         */
        post(endpoint, data = null) {
            return window.Ajax.request('POST', endpoint, data);
        },

        /**
         * Send a PATCH request
         *
         * @param  {string}   endpoint
         * @param  {Object}   data
         * @return {Promise}
         */
        patch(endpoint, data = null) {
            return window.Ajax.request('PATCH', endpoint, data);
        },

        /**
         * Send a PUT request
         *
         * @param  {string}   endpoint
         * @param  {Object}   data
         * @return {Promise}
         */
        put(endpoint, data = null) {
            return window.Ajax.request('PUT', endpoint, data);
        },

        /**
         * Send a DELETE request
         *
         * @param  {string}   endpoint
         * @param  {Object}   data
         * @return {Promise}
         */
        delete(endpoint, data = null) {
            return window.Ajax.request('DELETE', endpoint, data);
        }
    }
};
