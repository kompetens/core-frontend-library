import { findInputs } from "./InputResolver";
import { urlToEndpoint } from "../functions";
import get from "lodash/get";
import set from "lodash/set";

export default class Form {

    /**
     *
     * @param {Object} element
     */
    constructor(element) {
        if (element instanceof $ !== true) {
            throw new Error(`Invalid form element given - Must be a jQuery element.`);
        }

        let id = element.attr('id');

        // Store this form instance in the window object. => window._FORMS.{form-id}
        if (typeof id === 'string') {
            let existingForm = window.Form.getExistingInstance(id);
            if (existingForm instanceof window.Form) {
                delete this;
                return existingForm;

            } else {
                set(window, '_FORMS.' + id, this);
            }
        } else {
            console.warn('Form element missing ID, you will not be able to reference this form by name.');
        }

        // Ensure the form CSS class exists on the element.
        element.addClass('form');

        // Set base data
        this.form            = element;
        this.inputs          = findInputs(element);
        this.action          = element.attr('action');
        this.method          = element.attr('data-method') || 'POST';
        this.target          = element.attr('data-target') || null;
        this.successCallback = (data, message) => window.Alert.success(message);
        this.errorCallback   = () => null;
        this.modifyData      = null;
        this.requestedAt     = 0;
        this.disabled        = false;

        // Handle events for this form
        this.form.on('submit', this.submitForm.bind(this));

        // Set the error message container
        this.error = element.find('.form-error-container');
        if (!this.error.length) {
            this.error = $('<div class="form-error-container"></div>');
            this.form.prepend(this.error);
        }
        this.error.addClass('hidden');
    }

    /**
     * Named constructor to create a new form object based on the id of a form.
     *
     * @param  {String} id
     * @return {window.Form}
     */
    static fromId(id) {
        // Check for an existing Form instance first and return it instead of a new one.
        let existingForm = window.Form.getExistingInstance(id);
        if (existingForm instanceof window.Form) {
            return existingForm;
        }

        // Otherwise we create a new Form instance
        let element = $('#' + id);
        if (element.length === 0) {
            throw new Error(`Invalid element id "${id}", cannot setup form.`);
        }

        return new window.Form(element);
    }

    /**
     * Attempt to find a form instance
     *
     * @param  {string} id
     * @return {Form}
     */
    static getExistingInstance(id) {
        return get(window, '_FORMS.' + id, null);
    }

    /**
     * Register a callback function we call when the form has been submitted
     * and a response is ready to be returned to the user.
     *
     * @param {function} success
     * @param {function} error
     */
    registerCallback(success, error = null) {
        this.successCallback = success;

        if (error !== null) {
            this.errorCallback = error;
        }

        return this;
    }

    /**
     *
     * @param {string}      action
     * @param {string|null} target
     */
    setAction(action, target = null) {
        this.form.attr('action', action);
        this.action = action;
        if (target !== null) {
            this.target = target;
        }

        return this;
    }

    /**
     * Set data for the inputs of this form.
     *
     * @param {Object} data
     */
    setData(data) {
        this.inputs.forEach(input => {
            let value = get(data, input.name(), null);
            if (value !== null) {
                input.setValue(value);
            }
        });

        return this;
    }

    /**
     * Get the data from all inputs in this form
     *
     * @param  {?string} key
     * @return {Object}
     */
    getData(key = null) {
        let data = {};
        this.inputs.forEach(input => set(data, input.name(), input.value()));

        if (key !== null) {
            return get(data, key);
        }

        return data;
    }

    /**
     *
     * @return {boolean}
     */
    validate() {
        let isValid = true;

        this.inputs.forEach(input => {
            if (input.validate() !== true) {
                isValid = false;
            }
        });

        return isValid;
    }

    /**
     *
     * @param {Event} e
     * @param {Function} additionalCallback
     */
    submitForm(e = null, additionalCallback = null) {
        if (e !== null && typeof e !== 'undefined') {
            e.preventDefault();
        }

        if (this.disabled) {
            return;
        }

        // Make sure an action has been set.
        if (typeof this.action !== 'string' || this.action.length === 0) {
            throw new Error("No form action specified, cannot submit form.");
        }

        // Validate inputs, halting submission if validation fails.
        if (this.validate() !== true) {
            this.error.removeClass('hidden');
            this.error.text(trans('errors.validation'));
            return;
        }

        // Set the loading state.
        this.loadingState(true);

        const submitFunction = formData => {
            // Mark the form as being submitted and clear previous errors
            this.inputs.forEach(input => input.setErrors());
            this.error.text('');
            this.error.addClass('hidden');
            this.requestedAt = Date.now();

            // Send ajax request
            window.Ajax
                  .request(
                      this.method,
                      urlToEndpoint(this.action, this.target),
                      formData
                  )
                  .then(response => this.onSubmitted(response, additionalCallback))
                  .catch(this.onError.bind(this));
        };

        // Get the data, applying any modifiers
        let data = this.getData();
        if (this.modifyData === null) {
            submitFunction(data);
        } else {
            this.modifyData(data)
                .then(data => submitFunction(data))
                .catch(err => {
                    this.loadingState(false);
                    this.error.removeClass('hidden');
                    this.error.text(trans('errors.form_data_failed'));
                });
        }
    }

    /**
     * Handle successful form submissions
     *
     * @param {Object} response
     * @param {Function} additionalCallback
     */
    onSubmitted(response, additionalCallback = null) {
        let timeDiff     = Date.now() - this.requestedAt;
        let minimumDelay = 500; // Milliseconds the request must take before handling the response.

        if (timeDiff < minimumDelay) {
            setTimeout(() => this.onSubmitted(response, additionalCallback), minimumDelay - timeDiff + 10);
        } else {

            this.loadingState(false);
            this.successCallback(response.data, response.message);

            // Call any additional callbacks here
            if (additionalCallback !== null && typeof additionalCallback === 'function') {
                additionalCallback();
            }
        }
    }

    /**
     * Handle failed form submissions
     *
     * @param {Object} response
     * @param {number} status
     */
    onError(response, status) {
        let timeDiff     = Date.now() - this.requestedAt;
        let minimumDelay = 500; // Milliseconds the request must take before handling the response.

        if (timeDiff < minimumDelay) {
            setTimeout(() => this.onError(response, status), minimumDelay - timeDiff + 10);
        } else {

            this.loadingState(false);
            $("html, body").animate({scrollTop: this.form.offset().top - 75}, 400);

            if (status === 422) {
                let validation = response.data;
                this.inputs.forEach(input => input.setErrors(get(validation, input.name(), [])));
            }

            this.error.removeClass('hidden');
            this.error.text(response.message);

            // Trigger any user defined error callback.
            this.errorCallback(response, status);
        }
    }

    loadingState(state) {
        this.form.toggleClass('form--submitting', state);
    }
};
