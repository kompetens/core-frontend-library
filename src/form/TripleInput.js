import Input from "./Input";
import forOwn from "lodash/forOwn";

/**
 *
 */
export default class TripleInput extends Input {

    /**
     * Pass in the jquery element of the toggle "container"
     *
     * @param {*} el
     */
    constructor(el) {
        super(el);

        this.container = el.find('.tripe-input__container');
        this.renderToggle();

        // TODO: handle selector drag?
    }

    renderToggle() {
        this.container.html('<span class="triple-input__selector"></span>');
        let values = {
            'true': 'check',
            'null': 'help_outline',
            'false': 'clear'
        };

        forOwn(values, (icon, val) => {
            let option = $(`<i class="triple-input__option triple-input__${val} material-icons">${icon}</i>`);
            option.data('value', val);
            option.on('click', e => this.setValue(val));
            this.container.append(option);
        });
    }

    /**
     *
     * @return {string}
     */
    controlSelector() {
        return 'input[type="hidden"]';
    }

    /**
     *
     * @return {string}
     */
    value() {
        return this.input.val();
    }

    /**
     * Set a new value for the toggle. Automatically moves the selector to
     * update the visual of what is selected.
     *
     * @param   {string|boolean} val
     * @returns {boolean|null}
     */
    setValue(val) {
        // Parse the value
        let {value, strValue} = TripleInput.parsedValue(val);
        this.value            = value;

        // Reset css class value and add the new state.
        this.container.attr('class', 'tripe-input__container');
        this.container.addClass('tripe-input--' + strValue);
    }

    /**
     *
     * @param   {string|boolean} input
     * @returns {{value: *, strValue: string}}
     */
    static parsedValue(input) {
        let value    = null;
        let strValue = 'null';

        if (input === true || input === 'true') {
            value    = true;
            strValue = 'true';
        } else if (input === false || input === 'false') {
            value    = false;
            strValue = 'false';
        }

        return {value, strValue};
    }
}
