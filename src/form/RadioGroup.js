export default class RadioGroup {
    /**
     *
     * @param {string}       name
     * @param {Array<RadioInput>} inputs
     */
    constructor(name, inputs) {
        this.group  = name; // Cannot be named the same as a class method.
        this.inputs = inputs;
    }

    /**
     * Get the name of this group of radio inputs.
     *
     * @return {string}
     */
    name() {
        return this.group;
    }

    /**
     *
     * @return {string}
     */
    value() {
        let value = null;
        this.inputs.forEach(input => {
            if (input.isSelected()) {
                value = input.value();
            }
        });

        return value;
    }

    /**
     *
     * @param {string} value
     */
    setValue(value) {
        this.inputs.forEach(input => input.setValue(input.value() === value));
    }

    /**
     * Really no proper validation we can do here, so we return true.
     *
     * @return {boolean}
     */
    validate() {
        return true;
    }
}
