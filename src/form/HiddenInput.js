import Input from "./Input";

/**
 *
 */
export default class HiddenInput extends Input {

    /**
     *
     * @param {Object} element
     */
    constructor(element) {
        super(element);


        this.input = element;
    }

    /**
     * Since there are no visible error fields for a hidden input we simply do nothing.
     *
     * @param {Array} errors
     */
    setErrors(errors) {
        //
    }
}
