import Input from "./Input";
import { getFileAndSource } from "../functions";

/**
 *
 */
export default class FileInput extends Input {

    /**
     *
     * @param {Object} element
     */
    constructor(element) {
        super(element);
        this.file = null;
        this.src  = null;

        this.input.on('change', e => {
            getFileAndSource(e.target, ({file, src}) => {
                this.file = file;
                this.src  = src;
            });
        });
    }

    /**
     *
     * @return {*}
     */
    value() {
        return this.file;
    }
}
