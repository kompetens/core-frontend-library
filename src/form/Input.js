/**
 *
 */
export default class Input {

    /**
     *
     * @param {Object} element
     */
    constructor(element) {
        if (element instanceof $ !== true) {
            throw new Error(`Invalid input element given. "element" must be a jQuery element.`);
        }

        this.el     = element;
        this.input  = this.el.find(this.controlSelector());
        this.errors = this.el.find('.form-control-feedback');
    }

    /**
     *
     * @return {string}
     */
    controlSelector() {
        return '.form-control';
    }

    /**
     *
     * @return {string}
     */
    name() {
        return this.input.attr('name');
    }

    /**
     *
     * @return {string}
     */
    value() {
        return this.input.val();
    }

    /**
     *
     * @param {*} value
     */
    setValue(value) {
        if (this.input.type === 'number') {
            value = parseFloat(value);
        }

        this.input.val(value);
    }

    /**
     * Set or clear the errors for this input.
     * Currently we only display the first error message returned.
     *
     * @param {Array} errors
     */
    setErrors(errors = []) {
        let hasErrors = errors.length > 0;

        this.errors.text(hasErrors ? errors[0] : '');
        this.el.toggleClass('has-danger', hasErrors);
        this.input.toggleClass('is-invalid', hasErrors);
        this.errors.toggleClass('invalid-feedback', hasErrors);
    }

    /**
     * Allow attaching events to the input field.
     *
     * @param {string}   event
     * @param {Function} callback
     */
    on(event, callback) {
        this.input.on(event, callback);
    }

    /**
     * Validate the inputs. For now we only validate telephone inputs.
     * Other inputs have built in validation already.
     *
     * @return {boolean}
     */
    validate() {
        if (this.input.attr('type') === 'tel') {
            if (/^[ 0-9()+\-]*$/.test(this.value()) !== true) {
                this.setErrors([
                    trans('validation.phone', {
                        attribute: trans('validation.attributes.' + this.name())
                    })
                ]);
                return false;
            }
        }

        if (this.input.prop('required') && !this.value()) {
            this.setErrors([
                trans('validation.required', {
                    attribute: trans('validation.attributes.' + this.name())
                })
            ]);
            return false;
        }

        return true;
    }
}
