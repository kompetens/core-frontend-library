import Input from "./Input";

/**
 *
 */
export default class RadioInput extends Input {

    /**
     *
     * @return {string}
     */
    controlSelector() {
        return '.form-check-input';
    }

    /**
     * Is this radio selected?
     *
     * @return {boolean}
     */
    isSelected() {
        return this.input.attr('checked');
    }

    /**
     *
     * @param {boolean} checked
     */
    setValue(checked) {
        this.input.attr('checked', checked);
    }
}
