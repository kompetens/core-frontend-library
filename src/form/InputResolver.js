/**
 *
 * @param {Object} form
 */
import Input from "./Input";
import CheckboxInput from "./CheckboxInput";
import RadioInput from "./RadioInput";
import RadioGroup from "./RadioGroup";
import get from "lodash/get";
import forOwn from "lodash/forOwn";
import HiddenInput from "./HiddenInput";

export function findInputs(form) {

    let inputs = [];
    let radios = {};

    /**
     * We handle checkboxes and radios first since radios make an edge case by
     * having multiple "groups" be part of the same input "name". We process
     * them together because Bootstrap has them share the group CSS class.
     */
    form.find('.form-group, .form-check').each(function () {
        let group = $(this);

        /**
         * We need to first check for radio inputs since they make an edge case
         * by having multiple "groups" be part of the same input "name".
         */
        let radioInputs = group.find('input[type="radio"]');
        if (radioInputs.length) {
            let existingRadios = get(radios, radioInputs[0].name, []);
            existingRadios.push(new RadioInput(group));
            radios[radioInputs[0].name] = existingRadios;

        } else {
            /**
             * For the other inputs we simply use the resolveInput function.
             */
            let input = resolveInput(group);
            if (input instanceof Input) {
                inputs.push(input);
            }
        }
    });

    /**
     * Now that all input groups have been processed we can safely add the radio
     * inputs which are grouped by input name. We add all inputs for that group
     * to a new RadioGroup instance which handles the form data.
     *
     */
    forOwn(radios, (value, name) => {
        inputs.push(new RadioGroup(name, value));
    });

    /**
     * Finally search for all hidden inputs and add those to the form.
     */
    form.find('input[type=hidden]').each(function () {
        inputs.push(new HiddenInput($(this)))
    });

    return inputs;
}

/**
 * Resolve what type of input we are dealing with and return an appropriate
 * input class to handle getting and setting data as well as error messages.
 *
 * @param {Object} element
 */
export function resolveInput(element) {
    if (element instanceof $ !== true) {
        throw new Error(`Invalid input element given. "element" must be a jQuery element.`);
    }

    // Ignore file inputs for now
    if (element.find('input[type="file"]').length) {
        return;
    }

    if (element.find('input[type="checkbox"]').length) {
        return new CheckboxInput(element);
    }

    return new Input(element);
}
