import Input from "./Input";

/**
 *
 */
export default class CheckInput extends Input {

    /**
     *
     * @return {string}
     */
    controlSelector() {
        return '.form-check-input';
    }

    /**
     *
     * @return {boolean}
     */
    value() {
        return this.input.prop('checked');
    }

    /**
     *
     * @param {*} value
     */
    setValue(value) {
        this.input.prop('checked', value);
    }
}
