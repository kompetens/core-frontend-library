## vivatech core/partner frontend library
JS and Sass files for the core ecosystem.

Note: this repository needs to be public to be installable through npm/yarn

### installation

Preferred installation is through yarn to enable all developers to be on the exact same version.

`yarn add vivatech-core`

More documentation to be added
